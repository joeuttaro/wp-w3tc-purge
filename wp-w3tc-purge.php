<?php
/**
* Plugin Name: WP W3 Total Cache Purger
* Author:  Joe Uttaro
* Author URI: http://joeuttaro.com/
* Version: 0.1.2
* Description: Allows for users to flush all W3TC caches. See README.md for more details.
*/

// Register options page
add_action('admin_menu', 'wp_w3tc_purge_register_menu_page');
function wp_w3tc_purge_register_menu_page() {
    add_menu_page('Purge W3TC Cache', 'Purge Cache', 'purge_cache', 'wp-w3tc-purge', 'wp_w3tc_purge_menu_page', 'dashicons-image-rotate', 9999);
}

// Draw options page
function wp_w3tc_purge_menu_page() { ?>
    <div class="wrap">
        <?php screen_icon(); ?>
        <h2>Purge W3TC Cache</h2>
        <?php if (function_exists('w3tc_flush_all')){ ?>

            <?php if($_GET['purge'] == '1'){ ?>
                <?php w3tc_flush_all(); ?>

                <div class="notice notice-success is-dismissible">
                    <p>Cache purged.</p>
                </div>

            <?php } ?>
            <div class="notice notice-info">
                <p>
                    Clicking "Purge Cache" will purge the all W3TC caches. 
                    This may affect site performance as caches will need to be rebuilt. &nbsp; &nbsp;
                    <a class='button button-primary' href='?page=wp-w3tc-purge&purge=1' style="vertical-align: initial!important;">Purge Cache</a>
                </p>
            </div>

            <hr>

        <?php }else{ ?>
            <p>W3 Total Cache is not installed. Please install this plugin before continuing.</p>
        <?php } ?>
    </div>
<?php }

// Add capability on activate
register_activation_hook(__FILE__, 'wp_w3tc_purge_activate_plugin');
function wp_w3tc_purge_activate_plugin(){
    $role = get_role('administrator');
    $role->add_cap('purge_cache'); 
}
