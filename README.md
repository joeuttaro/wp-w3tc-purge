# WP W3 Total Cache Purger

WP W3 Total Cache Purger is a simple plugin that allows for a user to purge all W3 Total Cache caches. 

The plugin will add a custom capability to the site that allows for a developer to assign who is able to 
purge the site cache. By default, this capability will be added to the 'administrator' role. For more information 
on Wordpress capabilities, visit https://codex.wordpress.org/Roles_and_Capabilities

This plugin is dependant on having [W3 Total Cache](https://en-ca.wordpress.org/plugins/w3-total-cache/) installed.

## Changelog

### 0.1.2
Added custom capability `purge_cache` to manage the plugin.

### 0.1.1
Changed method of flushing cache from only flushing page cache, to flusing all caches. We are now using `w3tc_flush_all()`
instead of `w3tc_pgcache_flush()`.

### 0.1.0
Initial MVP completed. Updates made after this will be documented more thoroughly.

### 0.0.1
Initial plugin creation.